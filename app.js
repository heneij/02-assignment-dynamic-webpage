const loanButtonElement = document.getElementById("loanButton")
const payLoanButtonElement = document.getElementById("payLoanButton")
const balanceElement = document.getElementById("balance")
const loanElement = document.getElementById("loan")

const bankButtonElement = document.getElementById("bankButton")
const workButtonElement = document.getElementById("workButton")
const payElement = document.getElementById("pay")

const dropdownElement = document.getElementById("dropdownList")
const descriptionElement = document.getElementById("description")

const imageElement = document.getElementById("laptopImage")
const specsElement = document.getElementById("laptopSpecs")
const priceElement = document.getElementById("laptopPrice")
const buyButtonElement = document.getElementById("buyButton")

let laptops = []
let pay = 0.0
let balance = 0.0
let loan = 0.0
let selectedLaptop

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToCatalogue(laptops))

const addLaptopsToCatalogue = (laptops) => {
    laptops.forEach(x => addLapopToCatalogue(x))
    selectedLaptop = laptops[0]
    updateLaptop(selectedLaptop)
}

const addLapopToCatalogue = (laptop) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title))
    dropdownElement.appendChild(laptopElement)
}

const handleLaptopCatalogueChange = e => {
    selectedLaptop = laptops[e.target.selectedIndex]
    updateLaptop(selectedLaptop)
}
dropdownElement.addEventListener("change", handleLaptopCatalogueChange)

const updateLaptop = (laptop) => {
    priceElement.innerText = (laptop.price + ",-");
    descriptionElement.innerText = laptop.description
    specsElement.innerText = laptop.specs.join('\r\n')
    imageElement.src = laptop.image
}

const handleWork = e => {
    pay += 950.0
    payElement.innerText = pay + " kr"
}
workButtonElement.addEventListener("click", handleWork)

const handleBank = e => {
    if(loan > 0.1*pay){
        loan -= 0.1*pay
        balance += 0.9*pay
    }
    else{
        pay-=loan
        loan=0
        payLoanButtonElement.style.display = "none";
        balance += pay
    }
    pay = 0;
    payElement.innerText = pay + " kr"
    balanceElement.innerText = balance + " kr"
    loanElement.innerText = loan + " kr"
}
bankButtonElement.addEventListener("click", handleBank)

const handleGetLoan = e => {
    if(loan > 0){
        alert("It is not possible to get more than one loan before buying a computer")
        return
    }
    const desiredLoan = prompt("Insert desired loan amount: ")
    if(parseInt(desiredLoan) > balance*2){
        alert("It is not possible to borrow more than double your balance!")
    }
    else{
        loan += parseInt(desiredLoan)
        balance += parseInt(desiredLoan)
        balanceElement.innerText =  balance + " kr"
        payLoanButtonElement.style.display = "inline";
        loanElement.innerText = loan + " kr"
    }
}
loanButtonElement.addEventListener("click", handleGetLoan)

const handlePayLoan = e => {
    if(loan > pay){
        loan -= pay
        pay = 0;
    }
    else{
        pay-=loan
        loan=0
        payLoanButtonElement.style.display = "none";
    }
    payElement.innerText = pay + " kr"
    balanceElement.innerText = balance + " kr"
    loanElement.innerText = loan + " kr"
}
payLoanButtonElement.addEventListener("click", handlePayLoan)


const handleBuyButton = e => {
    // var selectedLaptop = laptops[e.target.selectedIndex]
    console.log("OOOBS!")
    if(selectedLaptop.price > balance){
        alert("Not enough funds available! Try to work and/or deposit to bank")
        return
    }
    balance -= selectedLaptop.price
    balanceElement.innerText = balance + " kr"
    alert("Congratulations, you are now the proud owner of a brand new " + selectedLaptop.title)
}
buyButtonElement.addEventListener("click", handleBuyButton)
